/*
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package com.assembla.nufco;

import java.io.File;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.netbeans.api.sendopts.CommandException;
import org.netbeans.spi.sendopts.Env;
import org.netbeans.spi.sendopts.Option;
import org.netbeans.spi.sendopts.OptionProcessor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.ViewCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.openide.windows.WindowManager;

/**
 *
 * @author Jerome Robert
 */
public class NOptionProcessor extends OptionProcessor
{
	private final static Option OPEN_OPTION = Option.requiredArgument('o', "open");
	private final static Option FS_OPTION = Option.withoutArgument('f', "fullscreen");
	@Override
	protected Set<Option> getOptions() {
		Set<Option> toReturn = new HashSet<Option>();
		toReturn.add(OPEN_OPTION);
		toReturn.add(FS_OPTION);
		return toReturn;
	}

	@Override
	protected void process(final Env env, Map<Option, String[]> options) throws CommandException {
		String[] o = options.get(OPEN_OPTION);
		if(o != null)
			open(o[0], env.getErrorStream());

		if(options.containsKey(FS_OPTION))
		{
			SwingUtilities.invokeLater(new Runnable(){
				public void run() {
					try
					{
						FullscreenAction.setFullScreenMode(true);
					}
					catch(Exception ex)
					{
						ex.printStackTrace(env.getOutputStream());
					}
				}
			});
		}
	}

	private void notifyNotFound(String filename, PrintStream out)
	{
		String msg = "Nufco cannot open " + filename;
		NotifyDescriptor d = new NotifyDescriptor.Message(msg,
			NotifyDescriptor.WARNING_MESSAGE);
		DialogDisplayer.getDefault().notifyLater(d);
		out.println("Warning: "+msg);
	}
	
	private void open(final String string, final PrintStream out) {
		
		FileObject fo = FileUtil.toFileObject(new File(string));
		if( fo == null)
			notifyNotFound(string, out);
		else
		{
			try {
				final ViewCookie v = DataObject.find(fo).getLookup().
					lookup(ViewCookie.class);
				if( v==null )
					notifyNotFound(string, out);
				else
					SwingUtilities.invokeLater(new Runnable(){
						public void run() {
							WindowManager.getDefault().invokeWhenUIReady(new Runnable(){
								public void run() {
									v.view();
								}
							});
						}
					});
			} catch (DataObjectNotFoundException ex) {
				Exceptions.printStackTrace(ex);
			}
		}
	}
}
