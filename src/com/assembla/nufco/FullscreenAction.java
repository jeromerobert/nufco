/*
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package com.assembla.nufco;

import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.lang.reflect.Method;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import org.openide.util.Exceptions;
import org.openide.windows.WindowManager;

public final class FullscreenAction extends AbstractAction {
	private static Method setMethod;
	private static JMenuBar menuBar;
	public FullscreenAction() {
		putValue(Action.SMALL_ICON,
			new ImageIcon(getClass().getResource("fullscreen.png")));
		putValue(Action.NAME, "Full screen");
		putValue(Action.SHORT_DESCRIPTION, "Full screen without menu bar");
	}

	public static void setFullScreenMode(boolean b)
	{
		Component[] cs;
		try {
			JFrame f = (JFrame)WindowManager.getDefault().getMainWindow();
			if (setMethod == null) {
				setMethod = f.getClass().getMethod("setFullScreenMode", Boolean.TYPE);
			}
			setMethod.invoke(f, b);
			if(b)
			{
				menuBar = f.getJMenuBar();
				f.setJMenuBar(null);
			}
			else
			{
				f.setJMenuBar(menuBar);
			}

		} catch (Exception ex) {
			Exceptions.printStackTrace(ex);
		}
	}
	
    public void actionPerformed(ActionEvent e)
	{
		try
		{
			Frame f = WindowManager.getDefault().getMainWindow();
			Method mis = f.getClass().getMethod("isFullScreenMode");
			Boolean b = (Boolean) mis.invoke(f);
			setFullScreenMode(!b);
		} catch (Exception ex) {
			Exceptions.printStackTrace(ex);
		}
    }
}
