/*
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package com.assembla.nufco;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jerome Robert
 */
public class SeriesTable extends JPanel
{
	private Chart series;
	private JTable table;
	private class MyTableModel extends DefaultTableModel
	{	
		@Override
		public int getRowCount() {
			if(series != null)
				return series.getSize();
			else
				return 0;
		}

		@Override
		public int getColumnCount() {
			return 3;
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			switch(columnIndex)
			{
				case 0: return String.class;
				case 1: return Color.class;
				case 2: return Boolean.class;
				default:
					throw new IllegalStateException();
			}
		}

		@Override
		public String getColumnName(int column) {
			switch(column)
			{
				case 0: return "Serie";
				case 1: return "Color";
				case 2: return "Visible";
				default:
					throw new IllegalStateException();
			}
		}

		@Override
		public Object getValueAt(int row, int column) {
			switch(column)
			{
				case 0: return series.getName(row);
				case 1: return series.getColor(row);
				case 2: return series.isVisible(row);
				default:
					throw new IllegalStateException();
			}
		}

		@Override
		public void setValueAt(Object aValue, int row, int column) {
			switch(column)
			{			
				case 1:
					series.setColor(row, (Color)aValue);
					break;
				case 2:
					series.setVisible(row, (Boolean)aValue);
					break;
				default:
					throw new IllegalStateException();
			}
		}

		@Override
		public boolean isCellEditable(int row, int column) {
			return column != 0;
		}
	}

	private class MyCellEditor extends DefaultCellEditor
	{
		public MyCellEditor() {
			super(new JCheckBox());
			editorComponent = new JButton();
			delegate = new EditorDelegate(){

				@Override
				public void setValue(Object value) {
					super.setValue(value);
					Color c = (Color) value;
					JButton b = (JButton) editorComponent;
					b.setForeground(c);
					b.setBackground(c);
				}

				@Override
				public void actionPerformed(ActionEvent e) {
					Color c = JColorChooser.showDialog(null, "Series color", (Color)value);
					if(c != null)
						value = c;
					MyCellEditor.this.stopCellEditing();
				}
			};
			((JButton)editorComponent).addActionListener(delegate);
		}
	}

	private void createTable()
	{
		this.table = new JTable();
		table.setModel(new MyTableModel());
		table.setDefaultEditor(Color.class, new MyCellEditor());
		table.setDefaultRenderer(Color.class, new DefaultTableCellRenderer()
		{
			@Override
			public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column)
			{

				Component toReturn = super.getTableCellRendererComponent(table,
					value, isSelected, hasFocus, row, column);
				Color c = (Color)value;
				toReturn.setBackground(c);
				toReturn.setForeground(c);
				return toReturn;
			}
		});
	}

	private void setAllVisibility(boolean b)
	{
		for(int i=0; i<series.getSize(); i++)
			series.setVisible(i, b);
		((MyTableModel) table.getModel()).fireTableDataChanged();
	}

	public SeriesTable(Chart series) {
		this.series = series;
		createTable();
		setLayout(new GridBagLayout());
		GridBagConstraints bg = new GridBagConstraints();
		bg.gridwidth = GridBagConstraints.REMAINDER;
		bg.fill = GridBagConstraints.BOTH;
		bg.weightx = 1.0;
		bg.weighty = 1.0;
		add(table, bg);

		bg = new GridBagConstraints();
		bg.gridx = 0;
		bg.gridy = 1;
		bg.weightx = 0.5;
		bg.insets = new Insets(5, 5, 5, 5);
		JButton allButton = new JButton("All");
		allButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				setAllVisibility(true);
			}
		});
		add(allButton, bg);

		bg = new GridBagConstraints();
		bg.gridx = 1;
		bg.gridy = 1;
		bg.weightx = 0.5;
		bg.insets = new Insets(5, 5, 5, 5);
		JButton noneButton = new JButton("None");
		noneButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				setAllVisibility(false);
			}
		});
		add(noneButton, bg);
	}
}
