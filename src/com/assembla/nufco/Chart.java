/*
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package com.assembla.nufco;

import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMResult;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Jerome Robert
 */

public class Chart {
	public final static String VISIBLE_PROPERTY = "Visible";
	public final static String COLOR_PROPERTY = "Color";
	public final static String AUTOSCALE_PROPERTY = "Auto-scale";

	/**
	 * @return the autoScale
	 */
	public boolean isAutoScale() {
		return autoScale;
	}

	/**
	 * @param autoScale the autoScale to set
	 */
	public void setAutoScale(boolean autoScale) {
		boolean old = this.autoScale;
		this.autoScale = autoScale;
		propertyChangeSupport.firePropertyChange(AUTOSCALE_PROPERTY, old, autoScale);
	}
		
	private class Series {
		public String name;
		public String csv;
		public int xCol;
		public int yCol;
		public boolean visible = true;
		public Color color;
		public double[][] data;

		public Series(String name, String csv, int xCol, int yCol) {
			this.name = name;
			this.csv = csv;
			this.xCol = xCol;
			this.yCol = yCol;
		}
	}

	private PropertyChangeSupport propertyChangeSupport =
		new PropertyChangeSupport(this);

	private Series[] series;
	private String xTitle = "X";
	private String yTitle = "Y";
	private String title;
	private boolean autoScale = true;
	
	public Chart(String title, InputStream in)
		throws IOException, TransformerException, SAXException
	{
		Element d = XMLTool.read(in).getDocumentElement();
		series = parseSeries(d);

		String xt = parseTitle(d, "x-axis");
		if(xt != null)
			xTitle = xt;
		
		String yt = parseTitle(d, "y-axis");
		if(yt != null)
			yTitle = yt;

		String t = d.getAttribute("title");
		if(t != null)
			this.title = t;
		else
			this.title = title;
	}

	public String getName(int i) {
		return series[i].name;
	}

	public boolean isVisible(int i) {
		return series[i].visible;
	}

	public double[][] getData(int i) {
		return series[i].data;
	}

	public Color getColor(int i) {
		return series[i].color;
	}

	public String getCsv(int i) {
		return series[i].csv;
	}

	public void setVisible(int i, boolean b) {
		boolean old = series[i].visible;
		series[i].visible = b;
		propertyChangeSupport.fireIndexedPropertyChange(VISIBLE_PROPERTY, i, old, b);	
	}

	public void addPropertyChangeListener(PropertyChangeListener listener)
	{
		propertyChangeSupport.addPropertyChangeListener(listener);
	}
	
	public void setColor(int i, Color color) {
		Color old = series[i].color;
		series[i].color = color;
		propertyChangeSupport.fireIndexedPropertyChange(COLOR_PROPERTY, i, old, color);
	}

	public int getSize()
	{
		return series.length;
	}

	private String parseTitle(Element e, String label)
	{
		NodeList nl = e.getElementsByTagName(label);
		if(nl.getLength() == 0)
			return null;
		else
		{
			Element et = (Element) nl.item(0);
			return et.getAttribute("title");
		}
	}
	
	private Series[] parseSeries(Element e)
	{
		NodeList nl = e.getElementsByTagName("series");
		Series[] toReturn = new Series[nl.getLength()];
		for(int i=0; i<nl.getLength(); i++)
			toReturn[i] = parseSerie((Element)nl.item(i));
		return toReturn;
	}
	
	private Series parseSerie(Element e)
	{
		String name = e.getAttribute("name");
		Element csv = (Element) e.getElementsByTagName("csv").item(0);
		int xCol = Integer.valueOf(csv.getAttribute("x-col"));
		int yCol = Integer.valueOf(csv.getAttribute("y-col"));
		
		Series toReturn = new Series(name, csv.getAttribute("uri"), xCol, yCol);
		NodeList nl = e.getElementsByTagName("color");
		if(nl.getLength() > 0)
			toReturn.color = parseColor((Element)nl.item(0));
		return toReturn;
	}
	
	private Color parseColor(Element element) {
		int r = Integer.parseInt(element.getAttribute("red"));
		int g = Integer.parseInt(element.getAttribute("green"));
		int b = Integer.parseInt(element.getAttribute("blue"));
		return new Color(r, g, b);
	}

	public void save(OutputStream os)
		throws TransformerException, SAXException, IOException
	{
		StringWriter sw = new StringWriter();
		try
		{
			XMLStreamWriter o = XMLTool.createXMLStreamWriter(sw);
			o.setDefaultNamespace("urn:nufco");
			o.writeStartElement("chart");
			o.setDefaultNamespace("urn:nufco");
			o.writeAttribute("title", title);

			o.writeStartElement("x-axis");
			o.writeAttribute("title", xTitle);
			o.writeEndElement();

			o.writeStartElement("y-axis");
			o.writeAttribute("title", yTitle);
			o.writeEndElement();

			for(Series s:series)
			{
				o.writeStartElement("series");
				o.writeAttribute("name", s.name);
				o.writeStartElement("csv");
				o.writeAttribute("uri", s.csv);
				o.writeAttribute("x-col", Integer.toString(s.xCol));
				o.writeAttribute("y-col", Integer.toString(s.yCol));
				o.writeEndElement();

				o.writeStartElement("color");
				o.writeAttribute("red", Integer.toString(s.color.getRed()));
				o.writeAttribute("green", Integer.toString(s.color.getGreen()));
				o.writeAttribute("blue", Integer.toString(s.color.getBlue()));
				o.writeEndElement();
				o.writeEndElement();
			}
			o.writeEndElement();
			o.close();
		}
		catch(XMLStreamException ex)
		{
			throw new IllegalStateException(ex);
		}
		XMLTool.write(sw.toString(), os);
	}

	public void readCSV(InputStream in, int seriesID) throws IOException
	{
		Series ser = series[seriesID];
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		String line = reader.readLine();
		ArrayList<double[]> list = new ArrayList<double[]>();
		while(line != null)
		{
			line = line.trim();
			if(!line.isEmpty())
			{
				String[] ss = line.split("[\\s]+|;");
				double[] d = new double[2];
				d[0] = Double.valueOf(ss[ser.xCol]);
				d[1] = Double.valueOf(ss[ser.yCol]);
				list.add(d);
			}
			line = reader.readLine();
		}
		ser.data = new double[2][list.size()];
		for(int i = 0; i<list.size(); i++)
		{
			ser.data[0][i] = list.get(i)[0];
			ser.data[1][i] = list.get(i)[1];
		}		
	}

	/**
	 * @return the xTitle
	 */
	public String getXTitle() {
		return xTitle;
	}

	/**
	 * @param xTitle the xTitle to set
	 */
	public void setXTitle(String xTitle) {
		this.xTitle = xTitle;
	}

	/**
	 * @return the yTitle
	 */
	public String getYTitle() {
		return yTitle;
	}

	/**
	 * @param yTitle the yTitle to set
	 */
	public void setYTitle(String yTitle) {
		this.yTitle = yTitle;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
}
