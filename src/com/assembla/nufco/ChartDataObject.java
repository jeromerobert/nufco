/*
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package com.assembla.nufco;

import java.awt.BorderLayout;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.swing.JScrollPane;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import org.jfree.chart.plot.DefaultDrawingSupplier;
import org.openide.cookies.SaveCookie;
import org.openide.cookies.ViewCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.text.DataEditorSupport;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import org.xml.sax.SAXException;

/**
 *
 * @author Jerome Robert
 */
public class ChartDataObject extends MultiDataObject {
	
	private class ChartTC extends TopComponent{
		private Chart chart;
		public ChartTC(Chart chart)
		{
			this.chart = chart;
			final NChartPanel panel = new NChartPanel(chart);
			setLayout(new BorderLayout());
			add(panel, BorderLayout.CENTER);
			chart.addPropertyChangeListener(new PropertyChangeListener() {

				public void propertyChange(PropertyChangeEvent evt) {
					panel.refresh();
					setModified(true);
				}
			});
			panel.refresh();
			setActivatedNodes(new Node[]{getNodeDelegate()});
		}

		@Override
		public int getPersistenceType() {
			return TopComponent.PERSISTENCE_NEVER;
		}

		@Override
		protected String preferredID() {
			return getClass().getName();
		}

		@Override
		public String getDisplayName() {
			return ChartDataObject.this.getName()+" - Chart";
		}

		@Override
		protected void componentClosed() {
			chart = null;
		}

		private Chart getSeriesGroup() {
			return chart;
		}
	}

	private class ArrayTC extends TopComponent {
		private Chart seriesGroup;

		private ArrayTC(Chart sg) {
			this.seriesGroup = sg;
			setLayout(new BorderLayout());
			JScrollPane pane = new JScrollPane();
			pane.getViewport().add(new SeriesTable(sg));
			add(pane, BorderLayout.CENTER);
		}

		@Override
		public int getPersistenceType() {
			return TopComponent.PERSISTENCE_NEVER;
		}

		@Override
		protected String preferredID() {
			return getClass().getName();
		}

		@Override
		public String getDisplayName() {
			return ChartDataObject.this.getName()+" - Series";
		}

		@Override
		protected void componentClosed() {
			array = null;
		}

		private Chart getSeriesGroup() {
			return seriesGroup;
		}
	}
	
	private ChartTC chart;
	private ArrayTC array;
	
	/** Return the chart if it exist, null else */
	public Chart getChart()
	{
		if(chart != null)
			return chart.getSeriesGroup();

		if(array != null)
			return array.getSeriesGroup();

		return null;
	}

	private void showTCs(Chart sg)
	{
		if (chart == null) {
			chart = new ChartTC(sg);
			Mode m = WindowManager.getDefault().findMode("editor");
			m.dockInto(chart);
		}

		if (array == null) {
			array = new ArrayTC(sg);
			Mode m = WindowManager.getDefault().findMode("properties");
			m.dockInto(array);
		}
		chart.open();
		chart.requestActive();
		array.open();
		array.requestActive();
	}
	public ChartDataObject(FileObject pf, MultiFileLoader loader)
			throws DataObjectExistsException, IOException
	{
		super(pf, loader);
		CookieSet cookies = getCookieSet();
		cookies.add(new ViewCookie() {

			public void view() {
				Chart sg = getChart();
				InputStream in = null;
				try {
					if (sg == null) {
						in = getPrimaryFile().getInputStream();
						sg = new Chart(getName(), in);
						readCSVFiles(sg);
					}
					showTCs(sg);
				} catch (TransformerException ex) {
					Exceptions.printStackTrace(ex);
				} catch (SAXException ex) {
					Exceptions.printStackTrace(ex);
				} catch (IOException ex) {
					Exceptions.printStackTrace(ex);
				} finally {
					try {
						if (in != null) {
							in.close();
						}
					} catch (IOException ex) {
						Exceptions.printStackTrace(ex);
					}
				}
			}
		});
		cookies.add((Node.Cookie) DataEditorSupport.create(
			this, getPrimaryEntry(), cookies));

		cookies.add(new SaveCookie() {
			public void save() throws IOException {
				try {
					OutputStream os = getPrimaryFile().getOutputStream();
					getChart().save(os);
					os.close();
					setModified(false);
				} catch (TransformerException ex) {
					Exceptions.printStackTrace(ex);
				} catch (SAXException ex) {
					Exceptions.printStackTrace(ex);
				}
			}
		});
	}

	private void readCSVFiles(Chart seriesGroup) throws IOException
	{
		DefaultDrawingSupplier dds = new DefaultDrawingSupplier();
		for(int i = 0; i < seriesGroup.getSize(); i++)
		{
			FileObject directory = getPrimaryFile().getParent();
			String csv = seriesGroup.getCsv(i);
			FileObject csvFile = directory.getFileObject(csv);
			if(csvFile == null)
				throw new IOException("Cannot find file "+csv+" in "+directory.getPath());
			InputStream in = csvFile.getInputStream();
			seriesGroup.readCSV(in, i);
			in.close();
			if(seriesGroup.getColor(i) == null)
				seriesGroup.setColor(i,(Color) dds.getNextPaint());
		}
	}
	@Override
	protected Node createNodeDelegate() {
		DataNode toReturn = (DataNode) super.createNodeDelegate();
		toReturn.setIconBaseWithExtension(ChartDataObject.class.getResource("icon.png").getPath());
		toReturn.setDisplayName(getPrimaryFile().getName());
		return toReturn;
	}

	@Override
	public Lookup getLookup() {
		return getCookieSet().getLookup();
	}
}
