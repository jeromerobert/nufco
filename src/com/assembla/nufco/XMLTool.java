/*
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package com.assembla.nufco;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.logging.Logger;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author Jerome Robert
 */
public class XMLTool {
    private final static Schema SCHEMA;
    private final static TransformerFactory XML_TRANSFORMER;
	private final static DocumentBuilder DOCUMENT_BUILDER;
	private final static Logger LOGGER = Logger.getLogger(XMLTool.class.getName());
	private final static XMLOutputFactory XML_OUTPUT_FACTORY =
		XMLOutputFactory.newInstance();

	static
	{
		try
		{
			SchemaFactory f =
					SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			SCHEMA = f.newSchema(XMLTool.class.getResource("nufco.xsd"));
		}
		catch (SAXException ex)
		{
			throw new IllegalStateException(ex);
		}

		XML_TRANSFORMER = TransformerFactory.newInstance();
		XML_TRANSFORMER.setAttribute("indent-number", 2);
		try
		{
			DOCUMENT_BUILDER = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		}
		catch (ParserConfigurationException ex)
		{
			throw new IllegalStateException(ex);
		}
		XML_OUTPUT_FACTORY.setProperty(
			XMLOutputFactory.IS_REPAIRING_NAMESPACES, true);
	}

	/** Create a DOMResult to be filled with XMLOutputStream */
	public static XMLStreamWriter createXMLStreamWriter(StringWriter sw)
			throws XMLStreamException
	{
		return XML_OUTPUT_FACTORY.createXMLStreamWriter(sw);
	}

	/** Write a DOMResult in utf-8*/
	public static void write(String buffer, OutputStream o)
		throws SAXException, TransformerException, IOException
    {        
        Transformer t = XML_TRANSFORMER.newTransformer();
        t.setOutputProperty(OutputKeys.INDENT, "yes");
        OutputStreamWriter osw = new OutputStreamWriter(o, "utf-8");
		t.transform(new StreamSource(new StringReader(buffer)), new StreamResult(osw));
		osw.flush();
		SCHEMA.newValidator().validate(new StreamSource(new StringReader(buffer)));
    }

    public static Document read(InputStream in)
        throws TransformerException, IOException, SAXException
    {
        Transformer t = TransformerFactory.newInstance().newTransformer();
        DOMResult result = new DOMResult();
        t.transform(new StreamSource(in), result);
        SCHEMA.newValidator().validate(new DOMSource(result.getNode()));
        return (Document)result.getNode();
    }

}
